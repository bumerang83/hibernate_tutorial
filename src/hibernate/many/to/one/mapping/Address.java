/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hibernate.many.to.one.mapping;

/*
 * @author    : bumerang    
 * Document   : Address
 * Created on : Mar 14, 2017, 10:05:39 AM
 */
// https://www.tutorialspoint.com/hibernate/hibernate_many_to_one_mapping.htm
/*
create table ADDRESS (
   id INT NOT NULL auto_increment,
   street_name VARCHAR(40) default NULL,
   city_name VARCHAR(40) default NULL,
   state_name VARCHAR(40) default NULL,
   zipcode VARCHAR(10) default NULL,
   PRIMARY KEY (id)
);
*/
public class Address {
   private int id;
   private String street;     
   private String city;     
   private String state;    
   private String zipcode; 

   public Address() {}
   public Address(String street, String city, 
                  String state, String zipcode) {
      this.street = street; 
      this.city = city; 
      this.state = state; 
      this.zipcode = zipcode; 
   }
   public int getId() {
      return id;
   }
   public void setId( int id ) {
      this.id = id;
   }
   public String getStreet() {
      return street;
   }
   public void setStreet( String street ) {
      this.street = street;
   }
   public String getCity() {
      return city;
   }
   public void setCity( String city ) {
      this.city = city;
   }
   public String getState() {
      return state;
   }
   public void setState( String state ) {
      this.state = state;
   }
   public String getZipcode() {
      return zipcode;
   }
   public void setZipcode( String zipcode ) {
      this.zipcode = zipcode;
   }
}
