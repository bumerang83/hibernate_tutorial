/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hibernate.sortedmap.mappings;

import java.util.*;

/**
 *
 * Author: bumerrrang
 * 
 * File:   Employee.java 
 *
 * Created on: 11-Dec-2016, 20:27:10
 *
 * Project name: HibernateTutorial
 *
 */
// https://www.tutorialspoint.com/hibernate/hibernate_sortedmap_mapping.htm
/*
create table EMPLOYEE (
   id INT NOT NULL auto_increment,
   first_name VARCHAR(20) default NULL,
   last_name  VARCHAR(20) default NULL,
   salary     INT  default NULL,
   PRIMARY KEY (id)
);
*/
public class Employee {
   private int id;
   private String firstName; 
   private String lastName;   
   private int salary;
   private SortedMap certificates;

   public Employee() {}
   public Employee(String fname, String lname, int salary) {
      this.firstName = fname;
      this.lastName = lname;
      this.salary = salary;
   }
   public int getId() {
      return id;
   }
   public void setId( int id ) {
      this.id = id;
   }
   public String getFirstName() {
      return firstName;
   }
   public void setFirstName( String first_name ) {
      this.firstName = first_name;
   }
   public String getLastName() {
      return lastName;
   }
   public void setLastName( String last_name ) {
      this.lastName = last_name;
   }
   public int getSalary() {
      return salary;
   }
   public void setSalary( int salary ) {
      this.salary = salary;
   }

   public SortedMap getCertificates() {
      return certificates;
   }
   public void setCertificates( SortedMap certificates ) {
      this.certificates = certificates;
   }
}
