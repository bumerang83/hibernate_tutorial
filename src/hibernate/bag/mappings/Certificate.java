/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hibernate.bag.mappings;

/**
 *
 * Author: bumerrrang
 * 
 * File:   Certificate.java 
 *
 * Created on: 28-Nov-2016, 10:52:12
 *
 * Project name: HibernateTutorial
 *
 */
// https://www.tutorialspoint.com/hibernate/hibernate_bag_mapping.htm
/* the being used CERTIFICATE table:
        create table CERTIFICATE (
           id INT NOT NULL auto_increment,
           certificate_name VARCHAR(30) default NULL,
           employee_id INT default NULL,
           PRIMARY KEY (id)
        );
*/
public class Certificate {
   private int id;
   private String name; 

   public Certificate() {}
   public Certificate(String name) {
      this.name = name;
   }
   public int getId() {
      return id;
   }
   public void setId( int id ) {
      this.id = id;
   }
   public String getName() {
      return name;
   }
   public void setName( String name ) {
      this.name = name;
   }
}
