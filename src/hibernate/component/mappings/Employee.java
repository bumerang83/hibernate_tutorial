/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hibernate.component.mappings;

/*
 * @author    : bumerang    
 * Document   : Employee
 * Created on : Mar 21, 2017, 11:17:10 AM
 */
// https://www.tutorialspoint.com/hibernate/hibernate_component_mappings.htm
/*
create table EMPLOYEE (
   id INT NOT  NULL auto_increment,
   first_name  VARCHAR(20) default NULL,
   last_name   VARCHAR(20) default NULL,
   salary      INT  default NULL,
   street_name VARCHAR(40) default NULL,
   city_name   VARCHAR(40) default NULL,
   state_name  VARCHAR(40) default NULL,
   zipcode     VARCHAR(10) default NULL,
   PRIMARY KEY (id)
);
*/
public class Employee implements java.io.Serializable {
   private int id;
   private String firstName; 
   private String lastName;   
   private int salary;
   private Address address;

   public Employee() {}
   public Employee(String fname, String lname, 
                   int salary, Address address ) {
      this.firstName = fname;
      this.lastName = lname;
      this.salary = salary;
      this.address = address;
   }
   public int getId() {
      return id;
   }
   public void setId( int id ) {
      this.id = id;
   }
   public String getFirstName() {
      return firstName;
   }
   public void setFirstName( String first_name ) {
      this.firstName = first_name;
   }
   public String getLastName() {
      return lastName;
   }
   public void setLastName( String last_name ) {
      this.lastName = last_name;
   }
   public int getSalary() {
      return salary;
   }
   public void setSalary( int salary ) {
      this.salary = salary;
   }

   public Address getAddress() {
      return address;
   }
   public void setAddress( Address address ) {
      this.address = address;
   }
}
