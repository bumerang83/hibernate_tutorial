/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hibernate.map.mappings;

/**
 *
 * Author: bumerrrang
 * 
 * File:   Certificate.java 
 *
 * Created on: 05-Dec-2016, 21:42:27
 *
 * Project name: HibernateTutorial
 *
 */
// https://www.tutorialspoint.com/hibernate/hibernate_map_mapping.htm
/*
create table CERTIFICATE (
   id INT NOT NULL auto_increment,
   certificate_type VARCHAR(40) default NULL,
   certificate_name VARCHAR(30) default NULL,
   employee_id INT default NULL,
   PRIMARY KEY (id)
);
*/
public class Certificate {
   private int id;
   private String name; 

   public Certificate() {}
   public Certificate(String name) {
      this.name = name;
   }
   public int getId() {
      return id;
   }
   public void setId( int id ) {
      this.id = id;
   }
   public String getName() {
      return name;
   }
   public void setName( String name ) {
      this.name = name;
   }
}
