/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hibernate.bag.mappings;

import java.util.Collection;

/**
 *
 * Author: bumerrrang
 * 
 * File:   Employee.java 
 *
 * Created on: 28-Nov-2016, 10:49:01
 *
 * Project name: HibernateTutorial
 *
 */
// https://www.tutorialspoint.com/hibernate/hibernate_bag_mapping.htm
/* the being used EMPLOYEE table:
        create table EMPLOYEE (
           id INT NOT NULL auto_increment,
           first_name VARCHAR(20) default NULL,
           last_name  VARCHAR(20) default NULL,
           salary     INT  default NULL,
           PRIMARY KEY (id)
        );
*/

public class Employee {
   private int id;
   private String firstName; 
   private String lastName;   
   private int salary;
   private Collection certificates;
   
   public Employee() {}
   public Employee(String fname, String lname, int salary) {
      this.firstName = fname;
      this.lastName = lname;
      this.salary = salary;
   }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the salary
     */
    public int getSalary() {
        return salary;
    }

    /**
     * @param salary the salary to set
     */
    public void setSalary(int salary) {
        this.salary = salary;
    }

    /**
     * @return the certificates
     */
    public Collection getCertificates() {
        return certificates;
    }

    /**
     * @param certificates the certificates to set
     */
    public void setCertificates(Collection certificates) {
        this.certificates = certificates;
    } 
}